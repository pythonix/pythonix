

import tkinter as tk
import customtkinter as ctk
from chapters import chapter1selector,chapter2selector,chapter3selector,chapter4selector,chapter5selector,chapter6selector,chapter7selector

progressions=[0,0,0,0,0,0,0,0,0,0]

class Dashboard(tk.Frame):
	class Chapter(tk.Frame):
		def __init__(self, parent,columnNum,rowNum,chapName,prog,fileName):
			super().__init__(master = parent)
			rectangle_chapitre = tk.Canvas(parent.mainFrame, width=parent.rectangle_chapitreImg.width(), height=parent.rectangle_chapitreImg.height(),highlightthickness=0)
			rectangle_chapitre.configure(bg="#002b36")
			rectangle_chapitre.create_image(0, 0, anchor=tk.NW, image=parent.rectangle_chapitreImg)
			rectangle_chapitre.grid(row=rowNum,column=columnNum) 
		
			chapitreFrame=tk.Frame(parent.mainFrame, width=parent.rectangle_chapitreImg.width(), height=parent.rectangle_chapitreImg.height(),highlightthickness=0)
			chapitreFrame.configure(bg="white")
			chapitreFrame.grid(row=rowNum,column=columnNum)
		
			tk.Label(chapitreFrame,text=chapName,font=parent.h3Font,background ='white',foreground='#002b36',justify="center",highlightthickness=0).pack(pady = (0, 30))
		
			pourcentage = tk.Canvas(chapitreFrame, width=parent.pourcentageImg.width(), height=parent.pourcentageImg.height(),highlightthickness=0)
			pourcentage.configure(bg= "white")
			pourcentage.create_image(0, 0, anchor=tk.NW, image=parent.pourcentageImg)
			pourcentage.create_image(0, 220-(prog*2.2), anchor=tk.NW, image=parent.pourcentage_reverseImg)
			pourcentage.pack(fill='y')
	


			tk.Label(chapitreFrame,text=str(prog)+'%',font=parent.h3Font,background ='white',foreground='#002b36',justify="center").pack()
		
			chapitreButton=tk.Button(chapitreFrame)
			if prog==0: 
				chapitreButton.configure(bg= "white",bd=0,image=parent.commencerImg,command=lambda:parent.change_page(fileName))
			else:
				chapitreButton.configure(bg= "white",bd=0,image=parent.continuerImg,command=lambda:parent.change_page(fileName))
			chapitreButton.pack(pady = (30, 0))	
		
	def __init__(self, master):
		super().__init__()
		
		# set main frame
		self.App = master
		self.mainFrame = tk.Frame(master=self.App)
		self.mainFrame.configure(bg='#002b36')
		self.mainFrame.pack(expand=True, fill='both')

		self.i = 0
		self.chapitres=['Introduction\n','Variables \net affectations','Arithmetique \net comparaisons','Conditions\n','Listes et Dictionnaires\n','Boucles\n','Fonctions\n']
		self.chapitresFiles=['chapter1','chapter2','chapter3','chapter4','chapter5','chapter6','chapter7']
		self.chapitresFilesTransform={'chapter1':chapter1selector,'chapter2':chapter2selector,'chapter3':chapter3selector,'chapter4':chapter4selector,'chapter5':chapter5selector,'chapter6':chapter6selector,'chapter7':chapter7selector
								}
		
		self.commencerImg = tk.PhotoImage(file='sources/assets/Dashboard/commencer.png')
		self.continuerImg = tk.PhotoImage(file='sources/assets/Dashboard/continuer.png')
		self.dashboardImg = tk.PhotoImage(file='sources/assets/Dashboard/dashboard.png')
		self.fleche_dImg = tk.PhotoImage(file='sources/assets/Dashboard/fleche_d.png')
		self.fleche_gImg = tk.PhotoImage(file='sources/assets/Dashboard/fleche_g.png')
		self.fondImg = tk.PhotoImage(file='sources/assets/Dashboard/fond.png')
		self.pourcentageImg= tk.PhotoImage(file='sources/assets/Dashboard/pourcentage.png')
		self.pourcentage_reverseImg = tk.PhotoImage(file='sources/assets/Dashboard/pourcentage_reverse.png')
		self.rectangle_chapitreImg = tk.PhotoImage(file='sources/assets/Dashboard/rectangle_chapitre.png')
		self.serpent_haut_gImg = tk.PhotoImage(file='sources/assets/Dashboard/serpent_haut_g.png')

		self.h3Font = ('Inter', 20, "bold")
		
		# set grid
		self.mainFrame.columnconfigure(0,weight=10)
		self.mainFrame.columnconfigure(1,weight=23)
		self.mainFrame.columnconfigure(2,weight=5)
		self.mainFrame.columnconfigure(3,weight=23)
		self.mainFrame.columnconfigure(4,weight=5)
		self.mainFrame.columnconfigure(5,weight=23)
		self.mainFrame.columnconfigure(6,weight=10)

		self.mainFrame.rowconfigure(0,weight=23)
		self.mainFrame.rowconfigure(1,weight=70)
		self.mainFrame.rowconfigure(2,weight=7)

		# content
		self.dashboard = tk.Canvas(self.mainFrame, width=self.dashboardImg.width(), height=self.dashboardImg.height(),highlightthickness=0)
		self.dashboard.create_image(0, 0, anchor=tk.NW, image=self.dashboardImg)
		self.dashboard.configure(bg="#002b36")
		self.dashboard.grid(row=0,column=0,columnspan=7,sticky='ns',pady=(50,0)) 
		
		# FLECHE GAUCHE
		self.fleche_g=tk.Button(self.mainFrame, bd=0, relief='flat', activebackground='#002b36', background='#002b36', image=self.fleche_gImg,command=lambda: self.move(-1))
		self.fleche_g.configure(bg="#002b36")
		self.fleche_g.grid(row=1,column=0) 

		# FLECHE DROITE
		self.fleche_d=tk.Button(self.mainFrame, relief='flat', bd=0, activebackground='#002b36', background='#002b36', image=self.fleche_dImg,command=lambda: self.move(1))
		self.fleche_d.configure(bg= "#002b36")
		self.fleche_d.grid(row=1,column=6)
		
		# CHAPITRE			
		self.Chapter(self,1,1,self.chapitres[0],progressions[0],self.chapitresFiles[0])
		self.Chapter(self,3,1,self.chapitres[1],progressions[1],self.chapitresFiles[1])
		self.Chapter(self,5,1,self.chapitres[2],progressions[2],self.chapitresFiles[2])


	def change_page(self, pageName):
		print(pageName)
		self.mainFrame.pack_forget()
		chapter=self.chapitresFilesTransform[pageName]
		chapter.ChapterFrame(self.master)
		
	def move(self,n):
		self.i += n
		if self.i<=len(self.chapitres)-3 and self.i>=0:
			self.Chapter(self,1,1,self.chapitres[self.i],progressions[self.i],self.chapitresFiles[self.i])
			self.Chapter(self,3,1,self.chapitres[self.i+1],progressions[self.i+1],self.chapitresFiles[self.i+1])
			self.Chapter(self,5,1,self.chapitres[self.i+2],progressions[self.i+2],self.chapitresFiles[self.i+2])
		else:
			self.i-=n
