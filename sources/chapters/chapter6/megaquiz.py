
unit_title = 'MEGA QUIZ'

h1Font = ('Inter', 54, "bold")
h2Font = ('Inter', 20, "bold")
from markdown import setTextWidget
import tkinter as tk
import customtkinter as ctk
import chapters.chapter7.unit1 as nextFrame
import chapters.chapter6selector as previousFrame
from dashboard import progressions
class Content(tk.Frame):
    def __init__(self, master: tk.Tk):
        super().__init__()
        self.root = master
        self.mainFrame = ctk.CTkScrollableFrame(master=self.root, fg_color='#D9D9D9', bg_color='#D9D9D9')
        self.mainFrame.pack(expand=True, fill='both')

        self.title=tk.Label(self.mainFrame, text='QUIZ',font=h1Font)
        self.title.configure(bg='#D9D9D9')
        self.title.pack(pady=10)

        self.valider=tk.PhotoImage(file='sources/assets/ElementDivers/valider.png').subsample(2,2)
        self.continuer=tk.PhotoImage(file='sources/assets/ElementDivers/continuer.png').subsample(2,2)
        self.retour=tk.PhotoImage(file='sources/assets/ElementDivers/retour.png').subsample(2,2)
        self.unit_content1 = '''**Determinez si l'annee de naissance est comprise entre 2010 et 2023**

listeDeNum=[1,2,3,4,5,6,7,8,9,10]
somme = 0
for  in range(1, (listeDeNum)):
    somme += ________
print("La somme des nombres de 1 à 10 est :", somme)

'''



        self.widget1 = tk.Text(self.mainFrame,height='20')
        setTextWidget(self.widget1, self.unit_content1, 'c')
        self.widget1.pack()

        self.bottomFrame1=tk.Frame(self.mainFrame)
        self.bottomFrame1.configure(bg="#D9D9D9")
        self.bottomFrame1.pack(pady=15)
        self.completionStatus1 = tk.StringVar(self.bottomFrame1)
        self.message1=tk.Label(self.bottomFrame1,textvariable=self.completionStatus1,font=h2Font)
        self.message1.configure(bg="#D9D9D9")
        self.message1.pack(side=tk.LEFT)
        self.validerButton1=tk.Button(self.bottomFrame1,image= self.valider,command=lambda: self.check(1),bd=0)
        self.validerButton1.configure(bg="#D9D9D9")
        self.validerButton1.pack(side=tk.LEFT,padx=15)

        self.unit_content2 = '''**Remplissez les trous dans le programme suivant afin qu'il affiche les éléments d'une liste de mots jusqu'à ce qu'il rencontre le mot "fin".**

mots = ["Bonjour", "Salut", "Hello", "Hi", "fin", "Hey"]
indice = 0

while mots[indice] != "":
    indice += 
'''

        self.widget2 = tk.Text(self.mainFrame,height='20')
        setTextWidget(self.widget2, self.unit_content2, 'c')
        self.widget2.pack()

        self.bottomFrame2=tk.Frame(self.mainFrame)
        self.bottomFrame2.configure(bg="#D9D9D9")
        self.bottomFrame2.pack(pady=15)
        self.completionStatus2 = tk.StringVar(self.bottomFrame2)

        self.message2=tk.Label(self.bottomFrame2,textvariable=self.completionStatus2,font=h2Font)
        self.message2.configure(bg="#D9D9D9")
        self.message2.pack(side=tk.LEFT)
        
        self.validerButton2=tk.Button(self.bottomFrame2,image= self.valider,command=lambda: self.check(2),bd=0)
        self.validerButton2.configure(bg="#D9D9D9")
        self.validerButton2.pack(side=tk.LEFT,padx=15)
        #PARAMETRES

        self.entry1 = tk.Entry(self.mainFrame)
        self.widget1.window_create(5.4, window=self.entry1)
        self.entry2 = tk.Entry(self.mainFrame)
        self.widget1.window_create(5.16, window=self.entry2)
        self.entry3 = tk.Entry(self.mainFrame)
        self.widget1.window_create(6.13, window=self.entry3)

        self.entry4 = tk.Entry(self.mainFrame)
        self.widget2.window_create(6.23, window=self.entry4)
        self.entry5 = tk.Entry(self.mainFrame)
        self.widget2.window_create(7.14, window=self.entry5)






        self.fullBottomFrame=tk.Frame(self.mainFrame)
        self.fullBottomFrame.configure(bg="#D9D9D9")
        self.fullBottomFrame.pack(pady=15)
        
        self.prochainButton=tk.Button(self.fullBottomFrame,image=self.continuer,command=self.nextPage,bd=0)
        self.prochainButton.configure(bg="#D9D9D9")
        self.retourButton=tk.Button(self.fullBottomFrame,image=self.retour,command=self.back,bd=0)
        self.retourButton.configure(bg="#D9D9D9")

        

    def check(self,exerciseName):
        if exerciseName==1:
            if "i" in self.entry1.get() and 'len' in self.entry2.get() and 'listeDeNum[i]' in self.entry3.get():
                self.completionStatus1.set("Correct!")
                self.message1.configure(fg='#00FF00')
            else:
                self.completionStatus1.set("Faux")
                self.message1.configure(fg='#ff0000')
        if exerciseName==2:
            if "1" in self.entry4.get() and 'fin' in self.entry5.get():
                self.completionStatus2.set("Correct!")
                self.message2.configure(fg='#00FF00')
            else:
                self.completionStatus2.set("Faux")
                self.message2.configure(fg='#ff0000')
        
        if self.completionStatus1.get()=='Correct!' and self.completionStatus2.get()=='Correct!':
            self.retourButton.pack(padx=15)
            self.prochainButton.pack(padx=15)
            
    def nextPage(self):
        progressions[5]+=round(100/11)
        nextFrame.Content(self.root)
        self.mainFrame.pack_forget()
    def back(self):
        previousFrame.ChapterFrame(self.root)
        self.mainFrame.pack_forget()
        
                
        